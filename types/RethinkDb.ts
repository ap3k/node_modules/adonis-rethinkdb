declare module '@ioc:RethinkDb' {
  import { R, RPoolConnectionOptions } from "rethinkdb-ts";

  export const r: R
  export const config: RPoolConnectionOptions
}

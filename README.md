# RethinkDB provider for AdonisJS v5

This is just RethinkDB provider.

Lucid-RethinkDb will follow after I get some more time

## Installation

1. Install package with `npm i adonis-rethinkdb` or `yarn add adonis-rethinkdb`

2. Add `"adonis-rethinkdb"` to your `providers` array in `.adonisrc.json`

3. Add `adonis-rethinkdb` to `tsconfig.json` types array

4. Create `config/rethinkdb.ts` config file and insert following config

    ```js
    const rethinkDb = {
      db: 'test',
      host: 'localhost',
      port: 28015,
      // tls: true,
      // ca: caCert,
      // rejectUnauthorized: false,
    }

    export default rethinkDb
    ```

5. Import `import { r } from '@ioc:RethinkDb'`

6. Run queries in default DB:

    ```js
    let players = await r.table('players').run()
    ```

7. Run queries in specified DB:

      ```js
      let players = await r.db('main').table('players').run()
      ```

import { IocContract } from '@adonisjs/fold'
import RethinkDb from '../src/RethinkDb'

export default class RethinkDbProvider {
  constructor(protected $container: IocContract) { }

  /**
   * Register binding
   */
  public register() {
    const config = this.$container.use('Adonis/Core/Config')
      .get('rethinkdb', {})
    let rethinkDb = new RethinkDb(config)

    this.$container.singleton('RethinkDb', () => rethinkDb)

    return rethinkDb.connect();
  }
}

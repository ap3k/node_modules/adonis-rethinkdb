import { r, R, RPoolConnectionOptions } from 'rethinkdb-ts'

export default class RethinkDb {
  public config: RPoolConnectionOptions

  public r: R

  constructor(config: RPoolConnectionOptions) {
    this.config = config
    this.r = r
  }

  /**
   * Initialize database connection pool
   */
  connect() {
    return r.connectPool(this.config)
  }
}
